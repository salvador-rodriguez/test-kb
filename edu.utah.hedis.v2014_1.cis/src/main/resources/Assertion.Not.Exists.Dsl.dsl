[when] Assertion.Not.Exists - There is NOT an assertion {ASSERTION:ENUM:Assertion.value} = 
(
$assertions_{ASSERTION} : java.util.List (size == 0) from collect ( Assertion(value == "{ASSERTION}") ) 
) //DslUsed==Assertion.Not.Exists.Dsl|||ASSERTION=={ASSERTION}