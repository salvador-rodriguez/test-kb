[when] Obs.Before.SubAdminOrder.Proximity.Time - Evaluated Person had {OBSFOCUS:ENUM:ObservationFocusConcept.openCdsConceptCode} with {HIGHLOW1:ENUM:TimeInterval.highLowUpper} time in the past {INT1} {TIMEUNITS1:ENUM:EnumerationTarget.javaCalendarUnitNoWeeks} which was followed by {MED:ENUM:MedicationConcept.openCdsConceptCode} with order {HIGHLOW2:ENUM:TimeInterval.highLowUpper} time within {INT2} {TIMEUNITS2:ENUM:EnumerationTarget.javaCalendarUnitNoWeeks} inclusive = 
(
// e.g.,  Evaluated Person had [Pregnancy Test] with [Low] time in the past [1 yr] which was followed by [Isoretinoin] with order [low] time within [7 days] inclusive

	$OBSAOPT_{OBSFOCUS}{MED}_preIds: java.util.List (size >= 1 ) from accumulate 
	( 
		ObservationFocusConcept
		( 
		openCdsConceptCode == "{OBSFOCUS}",
		$OBSAOPT_{OBSFOCUS}{MED}_preTargetId : conceptTargetId 
		),
	init (ArrayList $OBSAOPT_{OBSFOCUS}{MED}_tempPreIds = new ArrayList(); ),
	action ($OBSAOPT_{OBSFOCUS}{MED}_tempPreIds.add($OBSAOPT_{OBSFOCUS}{MED}_preTargetId); ),
	reverse ($OBSAOPT_{OBSFOCUS}{MED}_tempPreIds.remove($OBSAOPT_{OBSFOCUS}{MED}_preTargetId); ),
	result($OBSAOPT_{OBSFOCUS}{MED}_tempPreIds)
	) and

	$OBSAOPT_{OBSFOCUS}{MED}_postIds: java.util.List (size >= 1 ) from accumulate 
	( 
		MedicationConcept
		( 
		openCdsConceptCode == "{MED}",
		$OBSAOPT_{OBSFOCUS}{MED}_postTargetId : conceptTargetId 
		),
	init (ArrayList $OBSAOPT_{OBSFOCUS}{MED}_tempPostIds = new ArrayList(); ),
	action ($OBSAOPT_{OBSFOCUS}{MED}_tempPostIds.add($OBSAOPT_{OBSFOCUS}{MED}_postTargetId); ),
	reverse ($OBSAOPT_{OBSFOCUS}{MED}_tempPostIds.remove($OBSAOPT_{OBSFOCUS}{MED}_postTargetId); ),
	result($OBSAOPT_{OBSFOCUS}{MED}_tempPostIds)
	) and

	$OBSAOPT_{OBSFOCUS}{MED}_preDates: java.util.List (size >= 1 ) from accumulate 
	( 
		$OBSAOPT_{OBSFOCUS}{MED}_obsResult : ObservationResult
		( 
		evaluatedPersonId == $evaluatedPersonId, 
		id memberOf $OBSAOPT_{OBSFOCUS}{MED}_preIds,
		eval(timeBeforeByAtMost(observationEventTime.get{HIGHLOW1}(), $evalTime, {INT1}, {TIMEUNITS1}, namedObjects)),
		$OBSAOPT_{OBSFOCUS}{MED}_preDate : observationEventTime.get{HIGHLOW1}()
		),
	init (ArrayList $OBSAOPT_{OBSFOCUS}{MED}_tempPreDates = new ArrayList(); ),
	action ($OBSAOPT_{OBSFOCUS}{MED}_tempPreDates.add($OBSAOPT_{OBSFOCUS}{MED}_preDate); flagClinicalStatementToReturnInOutput($OBSAOPT_{OBSFOCUS}{MED}_obsResult); ),
	reverse ($OBSAOPT_{OBSFOCUS}{MED}_tempPreDates.remove($OBSAOPT_{OBSFOCUS}{MED}_preDate); flagClinicalStatementToNotReturnInOutput($OBSAOPT_{OBSFOCUS}{MED}_obsResult); ),
	result($OBSAOPT_{OBSFOCUS}{MED}_tempPreDates)
	) and 

	$OBSAOPT_{OBSFOCUS}{MED}_postDates: java.util.List (size >= 1 ) from accumulate 
	( 
		$OBSAOPT_{OBSFOCUS}{MED}_subAdminOrder : SubstanceAdministrationOrder
		( 
		evaluatedPersonId == $evaluatedPersonId, 
		id memberOf $OBSAOPT_{OBSFOCUS}{MED}_postIds,
		$OBSAOPT_{OBSFOCUS}{MED}_postDate : orderEventTime.get{HIGHLOW2}()
		),
	init (ArrayList $OBSAOPT_{OBSFOCUS}{MED}_tempPostDates = new ArrayList(); ),
	action ($OBSAOPT_{OBSFOCUS}{MED}_tempPostDates.add($OBSAOPT_{OBSFOCUS}{MED}_postDate); flagClinicalStatementToReturnInOutput($OBSAOPT_{OBSFOCUS}{MED}_subAdminOrder); ),
	reverse ($OBSAOPT_{OBSFOCUS}{MED}_tempPostDates.remove($OBSAOPT_{OBSFOCUS}{MED}_postDate); flagClinicalStatementToNotReturnInOutput($OBSAOPT_{OBSFOCUS}{MED}_subAdminOrder); ),
	result($OBSAOPT_{OBSFOCUS}{MED}_tempPostDates)
	) and	

	EvaluatedPerson(eval(timeAtOrBeforeByAtMost_ListEvaluation($OBSAOPT_{OBSFOCUS}{MED}_preDates, $OBSAOPT_{OBSFOCUS}{MED}_postDates, {INT2}, {TIMEUNITS2})))
) 
//DslUsed==Obs.Before.SubAdminOrder.Proximity.Time.Dsl|||OBSFOCUS=={OBSFOCUS}|||HIGHLOW1=={HIGHLOW1}|||INT1=={INT1}|||TIMEUNITS1=={TIMEUNITS1}|||MED=={MED}|||HIGHLOW2=={HIGHLOW2}|||INT2=={INT2}|||TIMEUNITS2=={TIMEUNITS2}