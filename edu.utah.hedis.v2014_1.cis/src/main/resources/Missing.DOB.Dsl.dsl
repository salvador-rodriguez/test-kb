[when] Missing.DOB - EvaluatedPerson is missing date of birth = 
(
  (EvaluatedPerson(id == $evaluatedPersonId, demographics == null)) 
or
  (EvaluatedPerson(id == $evaluatedPersonId, demographics.birthTime == null))
)
//DslUsed==Missing.DOB.Dsl
