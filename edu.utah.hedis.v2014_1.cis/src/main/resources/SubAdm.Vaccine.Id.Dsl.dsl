[when] SubAdm.Vaccine.Id - Evaluated Person had {SUBADMID:ENUM:SubstanceAdministrationId.reference} {VACCINE:ENUM:ImmunizationConcept.openCdsConceptCode} whose administration {HIGHLOW:ENUM:TimeInterval.highLowUpper} time was before eval time = 
( 
	$SubAdminVaccineIdDsl_immunizationConcept_{SUBADMID}{VACCINE} : ImmunizationConcept(
	openCdsConceptCode == "{VACCINE}") and 
	
	{SUBADMID} : SubstanceAdministrationEvent
	(
		id == $SubAdminVaccineIdDsl_immunizationConcept_{SUBADMID}{VACCINE}.conceptTargetId,
		administrationTimeInterval.get{HIGHLOW}() < $evalTime
	) and

	EvaluatedPerson(eval(flagClinicalStatementToReturnInOutput({SUBADMID})))	
) //DslUsed==SubAdm.Vaccine.Id.Dsl|||SUBADMID=={SUBADMID}|||VACCINE=={VACCINE}|||HIGHLOW=={HIGHLOW}