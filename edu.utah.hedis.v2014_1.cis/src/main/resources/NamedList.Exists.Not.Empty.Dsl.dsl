[when] NamedList.Exists.Not.Empty - The Named List {NAME} exists and is not empty = 
(
	NamedList
	(
		name == "{NAME}", 
		list != null,
		list.size() > 0,
		list == ( (List) (namedObjects.get("{NAME}")))
	)
) //DslUsed==NamedList.Exists.Not.Empty.Dsl|||NAME=={NAME}