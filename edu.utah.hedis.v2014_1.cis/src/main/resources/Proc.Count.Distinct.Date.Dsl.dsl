[when] Proc.Count.Distinct.Date - Evaluated Person had {PROC:ENUM:ProcedureConcept.openCdsConceptCode} with {HIGHLOW:ENUM:TimeInterval.highLowUpper} time before Eval Time {INT} or more times on distinct dates = 
(
// e.g.,  Evaluated Person had [Unilateral mastectomy] with [High] time before Eval Time [2] or more times on distinct dates

	$ProcCountDistinctDate_procIds_{PROC}{INT}: java.util.List (size >= {INT} ) from accumulate 
	( 
		ProcedureConcept
		( 
		openCdsConceptCode == "{PROC}",
		$ProcCountDistinctDate_procTargetId_{PROC}{INT} : conceptTargetId 
		),
	init (ArrayList $ProcCountDistinctDate_tempProcIds_{PROC}{INT} = new ArrayList(); ),
	action ($ProcCountDistinctDate_tempProcIds_{PROC}{INT}.add($ProcCountDistinctDate_procTargetId_{PROC}{INT}); ),
	reverse ($ProcCountDistinctDate_tempProcIds_{PROC}{INT}.remove($ProcCountDistinctDate_procTargetId_{PROC}{INT}); ),
	result($ProcCountDistinctDate_tempProcIds_{PROC}{INT})
	) and

	$ProcCountDistinctDate_procDates_{PROC}{INT}: java.util.List (size >= {INT} ) from accumulate 
	( 
		$ProcCountDistinctDate_proc_{PROC}{INT} : ProcedureEvent
		( 
		evaluatedPersonId == $evaluatedPersonId, 
		id memberOf $ProcCountDistinctDate_procIds_{PROC}{INT},
		procedureTime.get{HIGHLOW}() < $evalTime,
		$ProcCountDistinctDate_procDate_{PROC}{INT} : procedureTime.get{HIGHLOW}()
		),
	init (ArrayList $ProcCountDistinctDate_tempProcDates_{PROC}{INT} = new ArrayList(); ),
	action ($ProcCountDistinctDate_tempProcDates_{PROC}{INT}.add($ProcCountDistinctDate_procDate_{PROC}{INT}); flagClinicalStatementToReturnInOutput($ProcCountDistinctDate_proc_{PROC}{INT}); ),
	reverse ($ProcCountDistinctDate_tempProcDates_{PROC}{INT}.remove($ProcCountDistinctDate_procDate_{PROC}{INT}); flagClinicalStatementToNotReturnInOutput($ProcCountDistinctDate_proc_{PROC}{INT}); ),
	result($ProcCountDistinctDate_tempProcDates_{PROC}{INT})
	) and
		
	EvaluatedPerson(eval(countDistinctDates($ProcCountDistinctDate_procDates_{PROC}{INT}) >= {INT}))
) //DslUsed==Proc.Count.Distinct.Date.Dsl|||PROC=={PROC}|||HIGHLOW=={HIGHLOW}|||INT=={INT}