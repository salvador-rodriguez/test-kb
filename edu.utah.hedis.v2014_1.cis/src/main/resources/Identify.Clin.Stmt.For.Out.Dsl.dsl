[when] Identify.Clin.Stmt.For.Out - Identify Clinical Statements to be returned as a part of the output =
(
	// Clinical Statement is set to be returned in output and is identified as a root
	$cs : ClinicalStatement( toBeReturned == true, clinicalStatementToBeRoot == true ) and

	// There is a clinical statement relationship indicating this clinical statement is a child
	$csrP : ClinicalStatementRelationship( targetId == $cs.id, $csParentId : sourceId ) and

	// Parent Clinical Statement is also set to be returned and is identified as a root
	$csParent : ClinicalStatement( id == $csParentId, toBeReturned == true, clinicalStatementToBeRoot == true ) and

	// There is no child clinical statement that is set to be returned and is identified as a root
	not
	(
		$csrC : ClinicalStatementRelationship( sourceId == $cs.id, $csChildId : targetId ) and
		$csChild : ClinicalStatement( id == $csChildId, toBeReturned == true, clinicalStatementToBeRoot == true ) 
	)
) //DslUsed==Identify.Clin.Stmt.For.Out.Dsl