[when] SubDispense.Med.Time - Evaluated Person had {MED:ENUM:MedicationConcept.openCdsConceptCode} whose dispensation {HIGHLOW:ENUM:TimeInterval.highLowUpper} time was in the past {INT} {TIMEUNITS:ENUM:EnumerationTarget.javaCalendarUnitNoWeeks} = 
( 
	$SubDispenseMedTimeDsl_medConcept_{MED} : MedicationConcept(openCdsConceptCode == "{MED}") and 
	
	$SubDispenseMedTimeDsl_subDispenseEvent_{MED} : SubstanceDispensationEvent
	(
		id == $SubDispenseMedTimeDsl_medConcept_{MED}.conceptTargetId,
		eval(timeBeforeByAtMost(dispensationTime.get{HIGHLOW}(), $evalTime, {INT}, {TIMEUNITS}, namedObjects)) 
	) and 

	EvaluatedPerson(eval(flagClinicalStatementToReturnInOutput($SubDispenseMedTimeDsl_subDispenseEvent_{MED})))	
) //DslUsed==SubDispense.Med.Time.Dsl|||SUBADMID=={SUBADMID}|||MED=={MED}|||HIGHLOW=={HIGHLOW}|||INT=={INT}|||TIMEUNITS=={TIMEUNITS}