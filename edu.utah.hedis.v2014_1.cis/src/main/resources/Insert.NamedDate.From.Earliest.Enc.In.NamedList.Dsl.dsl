[then] Insert.NamedDate.From.Earliest.Enc.In.NamedList - Insert Named Date "{NAME}" for both Rules and Process which is the earliest encounter {HIGHLOW:ENUM:TimeInterval.highLowUpper} time in {NAMEDLISTNAME}; requires NamedList.Exists.Not.Empty = 
List<EncounterEvent> namedListEncs = (List<EncounterEvent>) namedObjects.get("{NAMEDLISTNAME}");
java.util.Date namedDateValue = getEarliestDateFromEncounterList(namedListEncs, "{HIGHLOW}");
namedObjects.put("{NAME}", namedDateValue);

// System.out.println(">> Adding: <{NAME}> with value " + namedDateValue.toString()); 

NamedDate $INDFEEINL_NamedDate = new NamedDate();
$INDFEEINL_NamedDate.setName("{NAME}");
$INDFEEINL_NamedDate.setDate(namedDateValue); 
insert ($INDFEEINL_NamedDate); //DslUsed==Insert.NamedDate.From.Earliest.Enc.In.NamedList.Dsl|||NAME=={NAME}|||HIGHLOW=={HIGHLOW}|||NAMEDLISTNAME=={NAMEDLISTNAME}