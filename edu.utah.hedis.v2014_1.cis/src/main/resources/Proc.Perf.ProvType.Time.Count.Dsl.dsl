[when] Proc.Perf.ProvType.Time.Count - Evaluated Person had at least {INT1} {PROC:ENUM:ProcedureConcept.openCdsConceptCode} by {ENTTYPE:ENUM:EntityTypeConcept.openCdsConceptCode} who is {ENTREL:ENUM:EntityRelationshipConcept.openCdsConceptCode} with {HIGHLOW:ENUM:TimeInterval.highLowUpper} time in the past {INT2} {TIMEUNITS:ENUM:EnumerationTarget.javaCalendarUnitNoWeeks} = 
(
	$ProcedureProviderTimeCountDsl_ProcedureConcept_{INT1}{PROC}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS} : java.util.List (size >= 0 ) from accumulate 
	( 
	ProcedureConcept
		( 
		openCdsConceptCode == "{PROC}",
		$ProcedureConceptTargetId_{INT1}{PROC}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS} : conceptTargetId 
		),
	init (ArrayList $ProcedureIds_{INT1}{PROC}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS} = new ArrayList(); ),
	action ($ProcedureIds_{INT1}{PROC}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS}.add($ProcedureConceptTargetId_{INT1}{PROC}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS}); ),
	reverse ($ProcedureIds_{INT1}{PROC}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS}.remove($ProcedureConceptTargetId_{INT1}{PROC}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS}); ),
	result($ProcedureIds_{INT1}{PROC}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS})
	) and  

	$ProcedureProviderTimeCountDsl_RelatedProviderConcept_{INT1}{PROC}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS} : java.util.List (size >= 0 ) from accumulate 
	( 
	EntityTypeConcept
		( 
		openCdsConceptCode == "{ENTTYPE}",
		$EntityTypeConceptTargetId_{INT1}{PROC}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS} : conceptTargetId 
		),
	init (ArrayList $EntityIds_{INT1}{PROC}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS} = new ArrayList(); ),
	action ($EntityIds_{INT1}{PROC}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS}.add($EntityTypeConceptTargetId_{INT1}{PROC}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS}); ),
	reverse ($EntityIds_{INT1}{PROC}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS}.remove($EntityTypeConceptTargetId_{INT1}{PROC}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS}); ),
	result($EntityIds_{INT1}{PROC}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS})
	) and  

	$ProcedureProviderTimeCountDsl_EntityRelationshipConcept_{INT1}{PROC}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS} : java.util.List (size >= 0 ) from accumulate 
	(
	EntityRelationshipConcept
		( 
		openCdsConceptCode == "{ENTREL}",
		$EntityRelationshipConceptTargetId_{INT1}{PROC}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS} : conceptTargetId 
		),
	init (ArrayList $ERCIds_{INT1}{PROC}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS} = new ArrayList(); ),
	action ($ERCIds_{INT1}{PROC}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS}.add($EntityRelationshipConceptTargetId_{INT1}{PROC}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS}); ),
	reverse ($ERCIds_{INT1}{PROC}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS}.remove($EntityRelationshipConceptTargetId_{INT1}{PROC}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS}); ),
	result($ERCIds_{INT1}{PROC}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS})
	) and  

	$ProcedureProviderTimeCountDsl_EntityRelationship_{INT1}{PROC}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS} : java.util.List (size >= 0 ) from accumulate 
	(
	EntityRelationship
		( 
		id memberOf $ProcedureProviderTimeCountDsl_EntityRelationshipConcept_{INT1}{PROC}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS},	
		$EntityRelationshipSourceId_{INT1}{PROC}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS} : sourceId memberOf $ProcedureProviderTimeCountDsl_ProcedureConcept_{INT1}{PROC}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS}, 
		targetEntityId memberOf $ProcedureProviderTimeCountDsl_RelatedProviderConcept_{INT1}{PROC}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS}
		),
	init (ArrayList $ERSourceIds_{INT1}{PROC}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS} = new ArrayList(); ),
	action ($ERSourceIds_{INT1}{PROC}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS}.add($EntityRelationshipSourceId_{INT1}{PROC}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS}); ),
	reverse ($ERSourceIds_{INT1}{PROC}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS}.remove($EntityRelationshipSourceId_{INT1}{PROC}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS}); ),
	result($ERSourceIds_{INT1}{PROC}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS})
	) and  

	$ProcedureProviderTimeCountDsl_ProcedureEventList_{INT1}{PROC}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS} : java.util.List( size >= {INT1} ) from collect 
	( 
	ProcedureEvent
		(
		evaluatedPersonId == $evaluatedPersonId, 
		id memberOf $ProcedureProviderTimeCountDsl_EntityRelationship_{INT1}{PROC}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS}, 		
		eval(timeBeforeByAtMost(procedureTime.get{HIGHLOW}(), $evalTime, {INT2}, {TIMEUNITS}, namedObjects))
		)
	) 
/* and
	
	EvaluatedPerson(eval(flagClinicalStatementListToReturnInOutput($ProcedureProviderTimeCountDsl_ProcedureEventList_{INT1}{PROC}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS})))
*/
) //DslUsed==Proc.Perf.ProvType.Time.Count.Dsl|||INT1=={INT1}|||PROC=={PROC}|||ENT_REL=={ENTREL}|||ENT_TYPE=={ENTTYPE}|||INT2=={INT2}|||TIMEUNITS=={TIMEUNITS}|||HIGHLOW=={HIGHLOW}