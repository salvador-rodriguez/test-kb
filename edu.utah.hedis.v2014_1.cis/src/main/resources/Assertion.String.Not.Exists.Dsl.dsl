[when] Assertion.String.Not.Exists - There is NOT an Assertion String {ASSERTIONSTRING} = 
(
   java.util.List (size == 0) from collect ( Assertion(value == "{ASSERTIONSTRING}") ) 
) //DslUsed==Assertion.String.Not.Exists.Dsl|||ASSERTIONSTRING=={ASSERTIONSTRING}