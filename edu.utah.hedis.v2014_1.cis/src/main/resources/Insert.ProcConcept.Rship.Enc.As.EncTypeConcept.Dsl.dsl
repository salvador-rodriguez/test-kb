[then] Insert.ProcConcept.Rship.Enc.As.EncTypeConcept - Insert identified Procedure Concept as Encounter's Encounter Type Concept for Rules; requires Read.ProcConcept.Rship.Enc =
EncounterTypeConcept  $encounterTypeConcept = new EncounterTypeConcept();
$encounterTypeConcept.setId((java.util.UUID.randomUUID()).toString());
$encounterTypeConcept.setConceptTargetId($RPCRE_encounterId);
$encounterTypeConcept.setOpenCdsConceptCode($RPCRE_procedureConcept.getOpenCdsConceptCode());
$encounterTypeConcept.setDeterminationMethodCode($RPCRE_procedureConcept.getDeterminationMethodCode());
// For some reason, getDisplayName() is not being recognized.  Not neededed so skipping.
// $encounterTypeConcept.setDisplayName($RPCRE_procedureConcept.getDisplayName());    
insert ($encounterTypeConcept); //DslUsed==Insert.ProcConcept.Rship.Enc.As.EncTypeConcept.Dsl