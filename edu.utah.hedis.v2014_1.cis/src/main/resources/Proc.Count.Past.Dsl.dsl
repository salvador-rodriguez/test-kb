[when] Proc.Count.Past - Evaluated Person had {PROC:ENUM:ProcedureConcept.openCdsConceptCode} with {HIGHLOW:ENUM:TimeInterval.highLowUpper} time before Eval Time {INT} or more times = 
(
	$ProcIds_{INT}{PROC} : java.util.List (size >= 0) from accumulate
	(              
		ProcedureConcept
		(              
			openCdsConceptCode == "{PROC}",
			$TempId : conceptTargetId
		),
		init (ArrayList $TempIds = new ArrayList(); ), action ($TempIds.add($TempId); ), reverse ($TempIds.remove($TempId); ), result($TempIds)
	) and
                                
	$Procs_{INT}{PROC} : java.util.List( size >= {INT} ) from collect 
	( 
		ProcedureEvent
		(
			evaluatedPersonId == $evaluatedPersonId, 
			procedureTime.get{HIGHLOW}() < $evalTime, 
			id memberOf $ProcIds_{INT}{PROC}         
		)
	)
	
) //DslUsed==Proc.Count.Past.Dsl|||PROC=={PROC}|||INT=={INT}|||HIGHLOW=={HIGHLOW}

