[then] Insert.NamedDate.Before.DOB - Insert Named Date "{NAME}" for both Rules and Process which is {INT} {TIMEUNITS:ENUM:EnumerationTarget.javaCalendarUnit} before DOB; requires Read.DOB = 
java.util.Date namedDateValue = org.opencds.common.utilities.DateUtility.getInstance().getDateAfterAddingTime( $ReadDOBDsl__DOB, {TIMEUNITS}, -1 * {INT});
namedObjects.put("{NAME}", namedDateValue);

NamedDate $namedDateBeforeDOB{INT}{TIMEUNITS} = new NamedDate();
$namedDateBeforeDOB{INT}{TIMEUNITS}.setName("{NAME}");
$namedDateBeforeDOB{INT}{TIMEUNITS}.setDate(namedDateValue); 
insert ($namedDateBeforeDOB{INT}{TIMEUNITS}); //DslUsed==Insert.NamedDate.Before.DOB.Dsl|||NAME=={NAME}|||INT=={INT}|||TIMEUNITS=={TIMEUNITS}