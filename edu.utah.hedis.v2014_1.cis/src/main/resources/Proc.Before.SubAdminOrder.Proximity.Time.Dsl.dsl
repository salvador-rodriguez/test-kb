[when] Proc.Before.SubAdminOrder.Proximity.Time - Evaluated Person had {PROC:ENUM:ProcedureConcept.openCdsConceptCode} with {HIGHLOW1:ENUM:TimeInterval.highLowUpper} time in the past {INT1} {TIMEUNITS1:ENUM:EnumerationTarget.javaCalendarUnitNoWeeks} which was followed by {MED:ENUM:MedicationConcept.openCdsConceptCode} with order {HIGHLOW2:ENUM:TimeInterval.highLowUpper} time within {INT2} {TIMEUNITS2:ENUM:EnumerationTarget.javaCalendarUnitNoWeeks} inclusive = 
(
// e.g.,  Evaluated Person had [Pregnancy Test] with [Low] time in the past [1 yr] which was followed by [Isoretinoin] with order [low] time within [7 days] inclusive

	$PBSAOPT_{PROC}{MED}_preIds : java.util.List (size >= 1 ) from accumulate 
	( 
		ProcedureConcept
		( 
		openCdsConceptCode == "{PROC}",
		$PBSAOPT_{PROC}{MED}_preTargetId : conceptTargetId 
		),
	init (ArrayList $PBSAOPT_{PROC}{MED}_tempPreIds = new ArrayList(); ),
	action ($PBSAOPT_{PROC}{MED}_tempPreIds.add($PBSAOPT_{PROC}{MED}_preTargetId); ),
	reverse ($PBSAOPT_{PROC}{MED}_tempPreIds.remove($PBSAOPT_{PROC}{MED}_preTargetId); ),
	result($PBSAOPT_{PROC}{MED}_tempPreIds)
	) and

	$PBSAOPT_{PROC}{MED}_postIds : java.util.List (size >= 1 ) from accumulate 
	( 
		MedicationConcept
		( 
		openCdsConceptCode == "{MED}",
		$PBSAOPT_{PROC}{MED}_postTargetId : conceptTargetId 
		),
	init (ArrayList $PBSAOPT_{PROC}{MED}_tempPostIds = new ArrayList(); ),
	action ($PBSAOPT_{PROC}{MED}_tempPostIds.add($PBSAOPT_{PROC}{MED}_postTargetId); ),
	reverse ($PBSAOPT_{PROC}{MED}_tempPostIds.remove($PBSAOPT_{PROC}{MED}_postTargetId); ),
	result($PBSAOPT_{PROC}{MED}_tempPostIds)
	) and

	$PBSAOPT_{PROC}{MED}_preDates : java.util.List (size >= 1 ) from accumulate 
	( 
		$PBSAOPT_{PROC}{MED}_proc : ProcedureEvent
		( 
		evaluatedPersonId == $evaluatedPersonId, 
		id memberOf $PBSAOPT_{PROC}{MED}_preIds,
		eval(timeBeforeByAtMost(procedureTime.get{HIGHLOW1}(), $evalTime, {INT1}, {TIMEUNITS1}, namedObjects)),
		$PBSAOPT_{PROC}{MED}_preDate : procedureTime.get{HIGHLOW1}()
		),
	init (ArrayList $PBSAOPT_{PROC}{MED}_tempPreDates = new ArrayList(); ),
	action ($PBSAOPT_{PROC}{MED}_tempPreDates.add($PBSAOPT_{PROC}{MED}_preDate); flagClinicalStatementToReturnInOutput($PBSAOPT_{PROC}{MED}_proc);),
	reverse ($PBSAOPT_{PROC}{MED}_tempPreDates.remove($PBSAOPT_{PROC}{MED}_preDate); flagClinicalStatementToNotReturnInOutput($PBSAOPT_{PROC}{MED}_proc); ),
	result($PBSAOPT_{PROC}{MED}_tempPreDates)
	) and 

	$PBSAOPT_{PROC}{MED}_postDates : java.util.List (size >= 1 ) from accumulate 
	( 
		$PBSAOPT_{PROC}{MED}_subAdminOrder : SubstanceAdministrationOrder
		( 
		evaluatedPersonId == $evaluatedPersonId, 
		id memberOf $PBSAOPT_{PROC}{MED}_postIds,
		$PBSAOPT_{PROC}{MED}_postDate : orderEventTime.get{HIGHLOW2}()
		),
	init (ArrayList $PBSAOPT_{PROC}{MED}_tempPostDates = new ArrayList(); ),
	action ($PBSAOPT_{PROC}{MED}_tempPostDates.add($PBSAOPT_{PROC}{MED}_postDate); flagClinicalStatementToReturnInOutput($PBSAOPT_{PROC}{MED}_subAdminOrder); ),
	reverse ($PBSAOPT_{PROC}{MED}_tempPostDates.remove($PBSAOPT_{PROC}{MED}_postDate); flagClinicalStatementToNotReturnInOutput($PBSAOPT_{PROC}{MED}_subAdminOrder); ),
	result($PBSAOPT_{PROC}{MED}_tempPostDates)
	) and	

	EvaluatedPerson(eval(timeAtOrBeforeByAtMost_ListEvaluation($PBSAOPT_{PROC}{MED}_preDates, $PBSAOPT_{PROC}{MED}_postDates, {INT2}, {TIMEUNITS2})))
) //DslUsed==Proc.Before.SubAdminOrder.Proximity.Time.Dsl|||PROC=={PROC}|||HIGHLOW1=={HIGHLOW1}|||INT1=={INT1}|||TIMEUNITS1=={TIMEUNITS1}|||MED=={MED}|||HIGHLOW2=={HIGHLOW2}|||INT2=={INT2}|||TIMEUNITS2=={TIMEUNITS2}