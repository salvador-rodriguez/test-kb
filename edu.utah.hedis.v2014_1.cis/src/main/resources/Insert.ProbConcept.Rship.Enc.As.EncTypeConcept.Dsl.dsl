[then] Insert.ProbConcept.Rship.Enc.As.EncTypeConcept - Insert identified Problem Concept as Encounter's Encounter Type Concept for Rules; requires Read.ProbConcept.Rship.Enc =
EncounterTypeConcept  $encounterTypeConcept = new EncounterTypeConcept();
$encounterTypeConcept.setId((java.util.UUID.randomUUID()).toString());
$encounterTypeConcept.setConceptTargetId($RPCRE_encounterId);
$encounterTypeConcept.setOpenCdsConceptCode($RPCRE_problemConcept.getOpenCdsConceptCode());
$encounterTypeConcept.setDeterminationMethodCode($RPCRE_problemConcept.getDeterminationMethodCode());
insert ($encounterTypeConcept); //DslUsed==Insert.ProbConcept.Rship.Enc.As.EncTypeConcept.Dsl