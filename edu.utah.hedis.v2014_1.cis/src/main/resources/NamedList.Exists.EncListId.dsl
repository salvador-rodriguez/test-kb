[when] NamedList.Exists.EncListId - The Named List {ENCLISTID:ENUM:EncounterListId.reference} {NAME} exists; may be empty = 
(
	NamedList
	(
		name == "{NAME}", 
		list != null,
		{ENCLISTID} : list == ( (List) (namedObjects.get("{NAME}")))
	)
) //DslUsed==NamedList.Exists.EncListId.Dsl|||ENCLISTID=={ENCLISTID}|||NAME=={NAME}