[when] EncDx.Between.NamedDates.NoTime - Evaluated Person had {PROB:ENUM:ProblemConcept.openCdsConceptCode} with {HIGHLOW:ENUM:TimeInterval.highLowUpper} time {COMP1:ENUM:Comparison.operator} {NAMEDDATE1NAME} and {COMP2:ENUM:Comparison.operator} {NAMEDDATE2NAME} ignoring time components of all dates = 
(
	$EDBNDNT_NamedDate1_{PROB}{HIGHLOW} : NamedDate
	(
		name == "{NAMEDDATE1NAME}", 
		date != null,
		$EDBNDNT_NamedDate1Value_{PROB}{HIGHLOW} : date == ((java.util.Date) namedObjects.get("{NAMEDDATE1NAME}"))
	) and

	$EDBNDNT_NamedDate2_{PROB}{HIGHLOW} : NamedDate
	(
		name == "{NAMEDDATE2NAME}", 
		date != null,
		$EDBNDNT_NamedDate2Value_{PROB}{HIGHLOW} : date == ((java.util.Date) namedObjects.get("{NAMEDDATE2NAME}"))
	) and
	
	ProblemConcept
	( 
		openCdsConceptCode == "{PROB}",
		$EDBNDNT_ProblemConceptTargetId_{PROB}{HIGHLOW} : conceptTargetId 
	) and  

	$EDBNDNT_Probem_{PROB}{HIGHLOW} : Problem
	(
		id == $EDBNDNT_ProblemConceptTargetId_{PROB}{HIGHLOW},
		eval((templateId != null) && java.util.Arrays.asList(templateId).contains("2.16.840.1.113883.3.1829.11.7.2.18")),
		evaluatedPersonId == $evaluatedPersonId, 
		(stripTimeComponent(problemEffectiveTime.get{HIGHLOW}())) {COMP1} (stripTimeComponent($EDBNDNT_NamedDate1Value_{PROB}{HIGHLOW})),
		(stripTimeComponent(problemEffectiveTime.get{HIGHLOW}())) {COMP2} (stripTimeComponent($EDBNDNT_NamedDate2Value_{PROB}{HIGHLOW})) 		
	) and 
	
	EvaluatedPerson
	(	
		eval(flagClinicalStatementToReturnInOutput($EDBNDNT_Probem_{PROB}{HIGHLOW}))
	)
) //DslUsed==EncDx.Between.NamedDates.NoTime.Dsl|||PROB=={PROB}|||HIGHLOW=={HIGHLOW}|||COMP1=={COMP1}|||NAMEDDATE1NAME=={NAMEDDATE1NAME}|||NAMEDDATE2NAME=={NAMEDDATE2NAME}