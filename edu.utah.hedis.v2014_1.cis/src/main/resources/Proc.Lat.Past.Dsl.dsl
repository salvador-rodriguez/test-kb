[when] Proc.Lat.Past - Evaluated Person had {PROC:ENUM:ProcedureConcept.openCdsConceptCode} with a laterality of {LAT:ENUM:ProcedureTargetBodySiteLateralityConcept.openCdsConceptCode} with {HIGHLOW:ENUM:TimeInterval.highLowUpper} time prior to Eval Time = 
(
	$ProcLatPastDsl_procedureConcept_{PROC}{LAT} : ProcedureConcept
	( 
		openCdsConceptCode == "{PROC}" 
	) and 
	
	$ProcLatPastDsl_lateralityConcept_{PROC}{LAT} : ProcedureTargetBodySiteLateralityConcept
	( 
		openCdsConceptCode == "{LAT}" 
	) and 
	
	$ProcLatPastDsl_procedure_{PROC}{LAT} : ProcedureEvent
	(
		id == $ProcLatPastDsl_procedureConcept_{PROC}{LAT}.conceptTargetId,
		id == $ProcLatPastDsl_lateralityConcept_{PROC}{LAT}.conceptTargetId,
		evaluatedPersonId == $evaluatedPersonId, 
		procedureTime.get{HIGHLOW}() < $evalTime
	) and
	
	EvaluatedPerson(eval(flagClinicalStatementToReturnInOutput($ProcLatPastDsl_procedure_{PROC}{LAT})))
	
) //DslUsed==Proc.Lat.Past.Dsl|||Proc=={PROC}|||LAT=={LAT}|||HIGHLOW=={HIGHLOW}