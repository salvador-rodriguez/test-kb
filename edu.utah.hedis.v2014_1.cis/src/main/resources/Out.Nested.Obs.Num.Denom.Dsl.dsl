[then] Out.Nested.Obs.Num.Denom - Create numerator value {NUMVALUE} as ID {NUMID} and denominator value {DENOMVALUE} as ID {DENOMID} for output observations focused on {OBSFOCUS:ENUM:ObservationFocusConcept.openCdsConceptCode} within enclosing observation ID {ROOT} = 
IVLDate obsTime{NUMID} = new IVLDate(); 
obsTime{NUMID}.setLow($evalTime); 
obsTime{NUMID}.setHigh($evalTime); 

ObservationResult childObs{NUMID} = new ObservationResult(); 
String childObs{NUMID}Id = "2.16.840.1.113883.3.795.5.1^{NUMID}"; 

childObs{NUMID}.setId(childObs{NUMID}Id); 
childObs{NUMID}.setEvaluatedPersonId($evaluatedPersonId); 
childObs{NUMID}.setObservationEventTime(obsTime{NUMID}); 
childObs{NUMID}.setSubjectIsFocalPerson($evaluatedPersonId == $focalPersonId); 
childObs{NUMID}.setClinicalStatementToBeRoot(false); 
childObs{NUMID}.setToBeReturned(true); 

CD childObs{NUMID}Focus = new CD(); 
childObs{NUMID}Focus.setCodeSystem("2.16.840.1.113883.3.795.12.1.1"); 
childObs{NUMID}Focus.setCodeSystemName("OpenCDS concepts"); 
childObs{NUMID}Focus.setCode("{OBSFOCUS}"); 
childObs{NUMID}Focus.setDisplayName(getOpenCDSConceptName("{OBSFOCUS}")); 
childObs{NUMID}.setObservationFocus(childObs{NUMID}Focus); 

int iChildObs{NUMID} = {NUMVALUE}; 
INT childObs{NUMID}IntegerValue = new INT(); 
childObs{NUMID}IntegerValue.setValue(iChildObs{NUMID}); 

ObservationValue childObs{NUMID}Value = new ObservationValue(); 
childObs{NUMID}Value.setInteger(childObs{NUMID}IntegerValue); 
childObs{NUMID}.setObservationValue(childObs{NUMID}Value); 

ClinicalStatementRelationship rel{NUMID} = new ClinicalStatementRelationship(); 
rel{NUMID}.setSourceId("2.16.840.1.113883.3.795.5.1^{ROOT}"); 
rel{NUMID}.setTargetId("2.16.840.1.113883.3.795.5.1^{NUMID}"); 

CD rel{NUMID}Code = new CD(); 
rel{NUMID}Code.setCodeSystem("2.16.840.1.113883.3.795.12.1.1"); 
rel{NUMID}Code.setCodeSystemName("OpenCDS concepts"); 
rel{NUMID}Code.setCode("C405"); 
rel{NUMID}Code.setDisplayName("Part of"); 
rel{NUMID}.setTargetRelationshipToSource(rel{NUMID}Code); 

java.util.List<RelationshipToSource> childObs{NUMID}RelationshipToSources = new java.util.ArrayList<RelationshipToSource>(); 
RelationshipToSource childObs{NUMID}RelationshipToSource = new RelationshipToSource(); 

childObs{NUMID}RelationshipToSource.setSourceId("2.16.840.1.113883.3.795.5.1^{ROOT}"); 
childObs{NUMID}RelationshipToSource.setRelationshipToSource(rel{NUMID}Code); 
childObs{NUMID}RelationshipToSources.add(childObs{NUMID}RelationshipToSource); 
childObs{NUMID}.setRelationshipToSources(childObs{NUMID}RelationshipToSources); 

insert(childObs{NUMID}); 
namedObjects.put("childObs{NUMID}", childObs{NUMID}); 

insert(rel{NUMID}); 
namedObjects.put("rel{NUMID}", rel{NUMID}); 

IVLDate obsTime{DENOMID} = new IVLDate(); 
obsTime{DENOMID}.setLow($evalTime); 
obsTime{DENOMID}.setHigh($evalTime); 

ObservationResult childObs{DENOMID} = new ObservationResult(); 
String childObs{DENOMID}Id = "2.16.840.1.113883.3.795.5.1^{DENOMID}"; 

childObs{DENOMID}.setId(childObs{DENOMID}Id); 
childObs{DENOMID}.setEvaluatedPersonId($evaluatedPersonId); 
childObs{DENOMID}.setObservationEventTime(obsTime{DENOMID}); 
childObs{DENOMID}.setSubjectIsFocalPerson($evaluatedPersonId == $focalPersonId); 
childObs{DENOMID}.setClinicalStatementToBeRoot(false); 
childObs{DENOMID}.setToBeReturned(true); 

CD childObs{DENOMID}Focus = new CD(); 
childObs{DENOMID}Focus.setCodeSystem("2.16.840.1.113883.3.795.12.1.1"); 
childObs{DENOMID}Focus.setCodeSystemName("OpenCDS concepts"); 
childObs{DENOMID}Focus.setCode("{OBSFOCUS}"); 
childObs{DENOMID}Focus.setDisplayName(getOpenCDSConceptName("{OBSFOCUS}")); 
childObs{DENOMID}.setObservationFocus(childObs{DENOMID}Focus); 

int iChildObs{DENOMID} = {DENOMVALUE}; 
INT childObs{DENOMID}IntegerValue = new INT(); 

childObs{DENOMID}IntegerValue.setValue(iChildObs{DENOMID}); 
ObservationValue childObs{DENOMID}Value = new ObservationValue(); 
childObs{DENOMID}Value.setInteger(childObs{DENOMID}IntegerValue); 
childObs{DENOMID}.setObservationValue(childObs{DENOMID}Value); 

ClinicalStatementRelationship rel{DENOMID} = new ClinicalStatementRelationship(); 
rel{DENOMID}.setSourceId("2.16.840.1.113883.3.795.5.1^{ROOT}"); 
rel{DENOMID}.setTargetId("2.16.840.1.113883.3.795.5.1^{DENOMID}"); 

CD rel{DENOMID}Code = new CD(); 
rel{DENOMID}Code.setCodeSystem("2.16.840.1.113883.3.795.12.1.1"); 
rel{DENOMID}Code.setCodeSystemName("OpenCDS concepts"); 
rel{DENOMID}Code.setCode("C405"); 
rel{DENOMID}Code.setDisplayName("Part of"); 
rel{DENOMID}.setTargetRelationshipToSource(rel{DENOMID}Code); 

java.util.List<RelationshipToSource> childObs{DENOMID}RelationshipToSources = new java.util.ArrayList<RelationshipToSource>(); 
RelationshipToSource childObs{DENOMID}RelationshipToSource = new RelationshipToSource(); 

childObs{DENOMID}RelationshipToSource.setSourceId("2.16.840.1.113883.3.795.5.1^{ROOT}"); 
childObs{DENOMID}RelationshipToSource.setRelationshipToSource(rel{DENOMID}Code); 
childObs{DENOMID}RelationshipToSources.add(childObs{DENOMID}RelationshipToSource); 
childObs{DENOMID}.setRelationshipToSources(childObs{DENOMID}RelationshipToSources); 

insert(childObs{DENOMID}); 
namedObjects.put("childObs{DENOMID}", childObs{DENOMID}); 

insert(rel{DENOMID}); 
namedObjects.put("rel{DENOMID}", rel{DENOMID}); //DslUsed==Out.Nested.Obs.Num.Denom.Dsl|||DENOMVALUE=={DENOMVALUE}|||DENOMID=={DENOMID}|||NUMVALUE=={NUMVALUE}|||NUMID=={NUMID}|||ROOT=={ROOT}|||OBSFOCUS=={OBSFOCUS}