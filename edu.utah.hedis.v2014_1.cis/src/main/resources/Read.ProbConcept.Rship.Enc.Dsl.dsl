[when] Read.ProbConcept.Rship.Enc - Get Problem Concept from Problem that is {CSREL:ENUM:ClinicalStatementRelationshipConcept.openCdsConceptCode} Encounter =
(
	//select a specific relationship concept
	$RPCRE_clinicalStatementRelationshipConcept : ClinicalStatementRelationshipConcept
	(
		openCdsConceptCode == "{CSREL}" 
	) and
	
	//join to relationships on that concept
	$RPCRE_clinicalStatementRelationship : ClinicalStatementRelationship 
	( 
		id == $RPCRE_clinicalStatementRelationshipConcept.conceptTargetId
		//$RPCRE_clinicalStatementRelationshipSourceId : sourceId,
		//targetId == $RPCRE_problemId
	) and

	//join to all problemConcepts
	$RPCRE_problemConcept : ProblemConcept  						
	(
	) and
	
	//where problems have a problemConcept
	Problem														
	(
		$RPCRE_problemId : id == $RPCRE_problemConcept.conceptTargetId,
		id == $RPCRE_clinicalStatementRelationship.targetId,
		evaluatedPersonId == $evaluatedPersonId
	) and
	
	//join to encounterEvents with specified relationship to problem
	EncounterEvent														
	(
		$RPCRE_encounterId : id == $RPCRE_clinicalStatementRelationship.sourceId, 
		evaluatedPersonId == $evaluatedPersonId,
		encounterEventTime.low < $evalTime
	) and not
	
	//where there is NOT an encounterTypeConcept matching problemConcept
	EncounterTypeConcept												 
	(
		conceptTargetId == $RPCRE_encounterId, 
		openCdsConceptCode == $RPCRE_problemConcept.openCdsConceptCode
	) 	
) //DslUsed==Read.ProbConcept.Rship.Enc.Dsl|||CSREL=={CSREL}