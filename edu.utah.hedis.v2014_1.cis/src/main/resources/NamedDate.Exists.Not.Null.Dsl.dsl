[when] NamedDate.Exists.Not.Null - The Named Date {NAME} exists and is not null = 
(
	NamedDate
	(
		name == "{NAME}", 
		date != null,
		date == ((java.util.Date) namedObjects.get("{NAME}"))
	)
) //DslUsed==NamedDate.Exists.Not.Null.Dsl|||NAME=={NAME}