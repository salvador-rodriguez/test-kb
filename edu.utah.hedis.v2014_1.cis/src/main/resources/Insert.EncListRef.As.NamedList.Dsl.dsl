[then] Insert.EncListRef.As.NamedList - Insert {ENCLISTID:ENUM:EncounterListId.reference} as Named List "{NAME}" for both Rules and Process = 
namedObjects.put("{NAME}", {ENCLISTID});

NamedList  $namedList{ENCLISTID} = new NamedList();
$namedList{ENCLISTID}.setName("{NAME}");
$namedList{ENCLISTID}.setList({ENCLISTID}); 
insert ($namedList{ENCLISTID}); //DslUsed==Insert.EncListRef.As.NamedList.Dsl|||ENCLISTID=={ENCLISTID}|||NAME=={NAME}

