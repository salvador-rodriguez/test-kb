[when] Obs.Time - Evaluated Person had {OBSFOCUS:ENUM:ObservationFocusConcept.openCdsConceptCode} with {HIGHLOW:ENUM:TimeInterval.highLowUpper} time in the past {INT} {TIMEUNITS:ENUM:EnumerationTarget.javaCalendarUnitNoWeeks} = 
(
	$ObsTimeDsl_observationFocusConcept_{OBSFOCUS} : ObservationFocusConcept
	(
	openCdsConceptCode == "{OBSFOCUS}"
	) and 

	$ObsTimeDsl_observationResult_{OBSFOCUS} : ObservationResult
	(
	id == $ObsTimeDsl_observationFocusConcept_{OBSFOCUS}.conceptTargetId, 
	evaluatedPersonId == $evaluatedPersonId, 
	eval(timeBeforeByAtMost(observationEventTime.get{HIGHLOW}(), $evalTime, {INT}, {TIMEUNITS}, namedObjects))
	) and
	
	EvaluatedPerson(eval(flagClinicalStatementToReturnInOutput($ObsTimeDsl_observationResult_{OBSFOCUS})))
) 
//DslUsed==Obs.Time.Dsl|||OBSFOCUS=={OBSFOCUS}|||INT=={INT}|||TIMEUNITS=={TIMEUNITS}|||HIGHLOW=={HIGHLOW}