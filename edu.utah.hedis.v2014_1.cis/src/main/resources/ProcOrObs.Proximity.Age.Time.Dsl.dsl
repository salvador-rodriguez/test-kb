[when] ProcOrObs.Proximity.Age.Time - If procedure or observation of {PROC1:ENUM:ProcedureConcept.openCdsConceptCode} and {PROC2:ENUM:ProcedureConcept.openCdsConceptCode} were done when patient was at least {INT1} years old and within {INT2} {TIMEUNITS2:ENUM:EnumerationTarget.javaCalendarUnitNoWeeks} of each other with {HIGHLOW:ENUM:TimeInterval.highLowUpper} time in the past {INT3} {TIMEUNITS3:ENUM:EnumerationTarget.javaCalendarUnitNoWeeks} = 
(
	${PROC1}{PROC2}{INT1}{INT2}{INT3}_ProcedureConcepts1 : java.util.List (size >= 0 ) from accumulate 
	( 
		ProcedureConcept
		( 
		openCdsConceptCode == "{PROC1}",
		${PROC1}{PROC2}{INT1}{INT2}{INT3}_ProcedureConceptsTargetId1 : conceptTargetId 
		),
	init (ArrayList ${PROC1}{PROC2}{INT1}{INT2}{INT3}_ProcedureIds1 = new ArrayList(); ),
	action (${PROC1}{PROC2}{INT1}{INT2}{INT3}_ProcedureIds1.add(${PROC1}{PROC2}{INT1}{INT2}{INT3}_ProcedureConceptsTargetId1); ),
	reverse (${PROC1}{PROC2}{INT1}{INT2}{INT3}_ProcedureIds1.remove(${PROC1}{PROC2}{INT1}{INT2}{INT3}_ProcedureConceptsTargetId1); ),
	result(${PROC1}{PROC2}{INT1}{INT2}{INT3}_ProcedureIds1)
	) and  

	${PROC1}{PROC2}{INT1}{INT2}{INT3}_ProcedureConcepts2 : java.util.List (size >= 0 ) from accumulate 
	( 
		ProcedureConcept
		( 
		openCdsConceptCode == "{PROC2}",
		${PROC1}{PROC2}{INT1}{INT2}{INT3}_ProcedureConceptsTargetId2 : conceptTargetId 
		),
	init (ArrayList ${PROC1}{PROC2}{INT1}{INT2}{INT3}_ProcedureIds2 = new ArrayList(); ),
	action (${PROC1}{PROC2}{INT1}{INT2}{INT3}_ProcedureIds2.add(${PROC1}{PROC2}{INT1}{INT2}{INT3}_ProcedureConceptsTargetId2); ),
	reverse (${PROC1}{PROC2}{INT1}{INT2}{INT3}_ProcedureIds2.remove(${PROC1}{PROC2}{INT1}{INT2}{INT3}_ProcedureConceptsTargetId2); ),
	result(${PROC1}{PROC2}{INT1}{INT2}{INT3}_ProcedureIds2)
	) and  

	${PROC1}{PROC2}{INT1}{INT2}{INT3}_ObservationFocusConcepts1 : java.util.List (size >= 0 ) from accumulate 
	( 
		ObservationFocusConcept
		( 
		openCdsConceptCode == "{PROC1}",
		${PROC1}{PROC2}{INT1}{INT2}{INT3}_ObservationFocusConceptsTargetId1 : conceptTargetId 
		),
	init (ArrayList ${PROC1}{PROC2}{INT1}{INT2}{INT3}_ObservationFocusIds1 = new ArrayList(); ),
	action (${PROC1}{PROC2}{INT1}{INT2}{INT3}_ObservationFocusIds1.add(${PROC1}{PROC2}{INT1}{INT2}{INT3}_ObservationFocusConceptsTargetId1); ),
	reverse (${PROC1}{PROC2}{INT1}{INT2}{INT3}_ObservationFocusIds1.remove(${PROC1}{PROC2}{INT1}{INT2}{INT3}_ObservationFocusConceptsTargetId1); ),
	result(${PROC1}{PROC2}{INT1}{INT2}{INT3}_ObservationFocusIds1)
	) and  

	${PROC1}{PROC2}{INT1}{INT2}{INT3}_ObservationFocusConcepts2 : java.util.List (size >= 0 ) from accumulate 
	( 
		ObservationFocusConcept
		( 
		openCdsConceptCode == "{PROC2}",
		${PROC1}{PROC2}{INT1}{INT2}{INT3}_ObservationFocusConceptsTargetId2 : conceptTargetId 
		),
	init (ArrayList ${PROC1}{PROC2}{INT1}{INT2}{INT3}_ObservationFocusIds2 = new ArrayList(); ),
	action (${PROC1}{PROC2}{INT1}{INT2}{INT3}_ObservationFocusIds2.add(${PROC1}{PROC2}{INT1}{INT2}{INT3}_ObservationFocusConceptsTargetId2); ),
	reverse (${PROC1}{PROC2}{INT1}{INT2}{INT3}_ObservationFocusIds2.remove(${PROC1}{PROC2}{INT1}{INT2}{INT3}_ObservationFocusConceptsTargetId2); ),
	result(${PROC1}{PROC2}{INT1}{INT2}{INT3}_ObservationFocusIds2)
	) and  

	${PROC1}{PROC2}{INT1}{INT2}{INT3}_ProcedureEvents1 : java.util.List( size >= 0 ) from collect 
	( 
	ProcedureEvent
		(
		evaluatedPersonId == $evaluatedPersonId, 
		id memberOf ${PROC1}{PROC2}{INT1}{INT2}{INT3}_ProcedureConcepts1,
		eval(timeBeforeByAtMost(procedureTime.get{HIGHLOW}(), $evalTime, {INT3}, {TIMEUNITS3}, namedObjects))
		)
	)  and 

	${PROC1}{PROC2}{INT1}{INT2}{INT3}_ProcedureEvents2 : java.util.List( size >= 0 ) from collect 
	( 
	ProcedureEvent
		(
		evaluatedPersonId == $evaluatedPersonId, 
		id memberOf ${PROC1}{PROC2}{INT1}{INT2}{INT3}_ProcedureConcepts2,
		eval(timeBeforeByAtMost(procedureTime.get{HIGHLOW}(), $evalTime, {INT3}, {TIMEUNITS3}, namedObjects))
		)
	)  and 

	${PROC1}{PROC2}{INT1}{INT2}{INT3}_ObservationResults1 : java.util.List( size >= 0 ) from collect 
	( 
	ObservationResult
		(
		evaluatedPersonId == $evaluatedPersonId, 
		id memberOf ${PROC1}{PROC2}{INT1}{INT2}{INT3}_ObservationFocusConcepts1,
		eval(timeBeforeByAtMost(observationEventTime.get{HIGHLOW}(), $evalTime, {INT3}, {TIMEUNITS3}, namedObjects))
		)
	)  and 

	${PROC1}{PROC2}{INT1}{INT2}{INT3}_ObservationResults2 : java.util.List( size >= 0 ) from collect 
	( 
	ObservationResult
		(
		evaluatedPersonId == $evaluatedPersonId, 
		id memberOf ${PROC1}{PROC2}{INT1}{INT2}{INT3}_ObservationFocusConcepts2,
		eval(timeBeforeByAtMost(observationEventTime.get{HIGHLOW}(), $evalTime, {INT3}, {TIMEUNITS3}, namedObjects))
		)
	) and 
	
	 EvaluatedPerson(id == $evaluatedPersonId, demographics != null, 
	demographics.birthTime!= null, 
	${PROC1}{PROC2}{INT1}{INT2}{INT3}_DOB : demographics.birthTime, 
	eval(proceduresOrObservationsMeetAgeReqAndAdjacent(${PROC1}{PROC2}{INT1}{INT2}{INT3}_DOB, $evalTime, {INT1}, {INT2}, {TIMEUNITS2}, ${PROC1}{PROC2}{INT1}{INT2}{INT3}_ProcedureEvents1, ${PROC1}{PROC2}{INT1}{INT2}{INT3}_ObservationResults1, ${PROC1}{PROC2}{INT1}{INT2}{INT3}_ProcedureEvents2, ${PROC1}{PROC2}{INT1}{INT2}{INT3}_ObservationResults2, "{HIGHLOW}")), 
	eval(flagClinicalStatementListToReturnInOutput(${PROC1}{PROC2}{INT1}{INT2}{INT3}_ProcedureEvents1)),
	eval(flagClinicalStatementListToReturnInOutput(${PROC1}{PROC2}{INT1}{INT2}{INT3}_ProcedureEvents2)),
	eval(flagClinicalStatementListToReturnInOutput(${PROC1}{PROC2}{INT1}{INT2}{INT3}_ObservationResults1)),
	eval(flagClinicalStatementListToReturnInOutput(${PROC1}{PROC2}{INT1}{INT2}{INT3}_ObservationResults2)))
	
	 // dob, evalTime, minAgeInYrs, maxSeparation, maxSeparationTimeUnits, procs1, obs1, procs2, obs2
	
) //DslUsed==ProcOrObs.Proximity.Age.Time.Dsl|||PROC1=={PROC1}|||PROC2=={PROC2}|||INT1=={INT1}|||INT2=={INT2}|||TIMEUNITS2=={TIMEUNITS2}|||INT3=={INT3}|||TIMEUNITS3=={TIMEUNITS3}