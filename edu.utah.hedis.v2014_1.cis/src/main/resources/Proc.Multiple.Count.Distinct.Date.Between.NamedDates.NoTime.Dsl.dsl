[when] Proc.Multiple.Count.Distinct.Date.Between.NamedDates.NoTime - Evaluated Person had {PROC1:ENUM:ProcedureConcept.openCdsConceptCode} {INT1} or more times and {PROC2:ENUM:ProcedureConcept.openCdsConceptCode} {INT2} or more times with all {HIGHLOW:ENUM:TimeInterval.highLowUpper} times {COMP1:ENUM:Comparison.operator} {NAMEDDATE1NAME} and {COMP2:ENUM:Comparison.operator} {NAMEDDATE2NAME} and all on distinct dates ignoring time components of all dates = 
(
// e.g.,  Evaluated Person had [DTaP] with [low] time [>=] [DOB_PLUS_42D] and [<=] [SecondBirthday] [1] or more times on distinct dates ignoring time components of all dates

	$PMCDDBNDNT_NamedDate1_{PROC1}{PROC2}{INT1}{INT2}  : NamedDate
	(
		name == "{NAMEDDATE1NAME}", 
		date != null,
		$PMCDDBNDNT_NamedDate1Value_{PROC1}{PROC2}{INT1}{INT2} : date == ((java.util.Date) namedObjects.get("{NAMEDDATE1NAME}"))
	) and

	$PMCDDBNDNT_NamedDate2_{PROC1}{PROC2}{INT1}{INT2} : NamedDate
	(
		name == "{NAMEDDATE2NAME}", 
		date != null,
		$PMCDDBNDNT_NamedDate2Value_{PROC1}{PROC2}{INT1}{INT2} : date == ((java.util.Date) namedObjects.get("{NAMEDDATE2NAME}"))
	) and
	
	$PMCDDBNDNT_Proc1Ids_{PROC1}{PROC2}{INT1}{INT2} : java.util.List (size >= {INT1} ) from accumulate 
	( 
		ProcedureConcept
		( 
		openCdsConceptCode == "{PROC1}",
		$PMCDDBNDNT_ProcConcept1TargetId_{PROC1}{PROC2}{INT1}{INT2} : conceptTargetId 
		),
	init (ArrayList $PMCDDBNDNT_TempProc1Ids_{PROC1}{PROC2}{INT1}{INT2} = new ArrayList(); ),
	action ($PMCDDBNDNT_TempProc1Ids_{PROC1}{PROC2}{INT1}{INT2}.add($PMCDDBNDNT_ProcConcept1TargetId_{PROC1}{PROC2}{INT1}{INT2}); ),
	reverse ($PMCDDBNDNT_TempProc1Ids_{PROC1}{PROC2}{INT1}{INT2}.remove($PMCDDBNDNT_ProcConcept1TargetId_{PROC1}{PROC2}{INT1}{INT2}); ),
	result($PMCDDBNDNT_TempProc1Ids_{PROC1}{PROC2}{INT1}{INT2})
	) and
	
	$PMCDDBNDNT_Proc2Ids_{PROC1}{PROC2}{INT1}{INT2} : java.util.List (size >= {INT1} ) from accumulate 
	( 
		ProcedureConcept
		( 
		openCdsConceptCode == "{PROC2}",
		$PMCDDBNDNT_ProcConcept2TargetId_{PROC1}{PROC2}{INT1}{INT2} : conceptTargetId 
		),
	init (ArrayList $PMCDDBNDNT_TempProc2Ids_{PROC1}{PROC2}{INT1}{INT2} = new ArrayList(); ),
	action ($PMCDDBNDNT_TempProc2Ids_{PROC1}{PROC2}{INT1}{INT2}.add($PMCDDBNDNT_ProcConcept2TargetId_{PROC1}{PROC2}{INT1}{INT2}); ),
	reverse ($PMCDDBNDNT_TempProc2Ids_{PROC1}{PROC2}{INT1}{INT2}.remove($PMCDDBNDNT_ProcConcept2TargetId_{PROC1}{PROC2}{INT1}{INT2}); ),
	result($PMCDDBNDNT_TempProc2Ids_{PROC1}{PROC2}{INT1}{INT2})
	) and
		
	$PMCDDBNDNT_Proc1Dates_{PROC1}{PROC2}{INT1}{INT2} : java.util.List (size >= {INT1} ) from accumulate 
	( 
		$PMCDDBNDNT_Proc1_{PROC1}{PROC2}{INT1}{INT2} : ProcedureEvent
		( 
		evaluatedPersonId == $evaluatedPersonId, 
		id memberOf $PMCDDBNDNT_Proc1Ids_{PROC1}{PROC2}{INT1}{INT2},
		(stripTimeComponent(procedureTime.get{HIGHLOW}())) {COMP1} (stripTimeComponent($PMCDDBNDNT_NamedDate1Value_{PROC1}{PROC2}{INT1}{INT2})),
		(stripTimeComponent(procedureTime.get{HIGHLOW}())) {COMP2} (stripTimeComponent($PMCDDBNDNT_NamedDate2Value_{PROC1}{PROC2}{INT1}{INT2})),
		$PMCDDBNDNT_Proc1Date_{PROC1}{PROC2}{INT1}{INT2} : procedureTime.get{HIGHLOW}()
		),
	init (ArrayList $PMCDDBNDNT_TempProc1Dates_{PROC1}{PROC2}{INT1}{INT2} = new ArrayList(); ),
	action ($PMCDDBNDNT_TempProc1Dates_{PROC1}{PROC2}{INT1}{INT2}.add($PMCDDBNDNT_Proc1Date_{PROC1}{PROC2}{INT1}{INT2}); flagClinicalStatementToReturnInOutput($PMCDDBNDNT_Proc1_{PROC1}{PROC2}{INT1}{INT2}); ),
	reverse ($PMCDDBNDNT_TempProc1Dates_{PROC1}{PROC2}{INT1}{INT2}.remove($PMCDDBNDNT_Proc1Date_{PROC1}{PROC2}{INT1}{INT2}); flagClinicalStatementToNotReturnInOutput($PMCDDBNDNT_Proc1_{PROC1}{PROC2}{INT1}{INT2}); ),
	result($PMCDDBNDNT_TempProc1Dates_{PROC1}{PROC2}{INT1}{INT2})
	) and
		
	$PMCDDBNDNT_Proc2Dates_{PROC1}{PROC2}{INT1}{INT2} : java.util.List (size >= {INT1} ) from accumulate 
	( 
		$PMCDDBNDNT_Proc2_{PROC1}{PROC2}{INT1}{INT2} : ProcedureEvent
		( 
		evaluatedPersonId == $evaluatedPersonId, 
		id memberOf $PMCDDBNDNT_Proc2Ids_{PROC1}{PROC2}{INT1}{INT2},
		(stripTimeComponent(procedureTime.get{HIGHLOW}())) {COMP1} (stripTimeComponent($PMCDDBNDNT_NamedDate1Value_{PROC1}{PROC2}{INT1}{INT2})),
		(stripTimeComponent(procedureTime.get{HIGHLOW}())) {COMP2} (stripTimeComponent($PMCDDBNDNT_NamedDate2Value_{PROC1}{PROC2}{INT1}{INT2})),
		$PMCDDBNDNT_Proc2Date_{PROC1}{PROC2}{INT1}{INT2} : procedureTime.get{HIGHLOW}()
		),
	init (ArrayList $PMCDDBNDNT_TempProc2Dates_{PROC1}{PROC2}{INT1}{INT2} = new ArrayList(); ),
	action ($PMCDDBNDNT_TempProc2Dates_{PROC1}{PROC2}{INT1}{INT2}.add($PMCDDBNDNT_Proc2Date_{PROC1}{PROC2}{INT1}{INT2}); flagClinicalStatementToReturnInOutput($PMCDDBNDNT_Proc2_{PROC1}{PROC2}{INT1}{INT2}); ),
	reverse ($PMCDDBNDNT_TempProc2Dates_{PROC1}{PROC2}{INT1}{INT2}.remove($PMCDDBNDNT_Proc2Date_{PROC1}{PROC2}{INT1}{INT2}); flagClinicalStatementToNotReturnInOutput($PMCDDBNDNT_Proc2_{PROC1}{PROC2}{INT1}{INT2}); ),
	result($PMCDDBNDNT_TempProc2Dates_{PROC1}{PROC2}{INT1}{INT2})
	) and
	
	EvaluatedPerson(eval(dateListsFulfillDistinctDateCountCriteria($PMCDDBNDNT_Proc1Dates_{PROC1}{PROC2}{INT1}{INT2}, $PMCDDBNDNT_Proc2Dates_{PROC1}{PROC2}{INT1}{INT2}, {INT1}, {INT2})))
) //DslUsed==Proc.Multiple.Count.Distinct.Date.Between.NamedDates.NoTime.Dsl|||PROC1=={PROC1}|||INT1=={INT1}|||PROC2=={PROC2}|||INT2=={INT2}|||HIGHLOW=={HIGHLOW}|||COMP1=={COMP1}|||NAMEDDATE1NAME=={NAMEDDATE1NAME}|||COMP2=={COMP2}|||NAMEDDATE2NAME=={NAMEDDATE2NAME}