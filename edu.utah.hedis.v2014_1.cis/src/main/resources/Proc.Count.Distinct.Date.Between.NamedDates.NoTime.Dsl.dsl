[when] Proc.Count.Distinct.Date.Between.NamedDates.NoTime - Evaluated Person had {PROC:ENUM:ProcedureConcept.openCdsConceptCode} with {HIGHLOW:ENUM:TimeInterval.highLowUpper} time {COMP1:ENUM:Comparison.operator} {NAMEDDATE1NAME} and {COMP2:ENUM:Comparison.operator} {NAMEDDATE2NAME} {INT} or more times on distinct dates ignoring time components of all dates = 
(
// e.g.,  Evaluated Person had [DTaP] with [low] time [>=] [DOB_PLUS_42D] and [<=] [SecondBirthday] [1] or more times on distinct dates ignoring time components of all dates

	$PCDDBNDNT_NamedDate1_{PROC}{INT}  : NamedDate
	(
		name == "{NAMEDDATE1NAME}", 
		date != null,
		$PCDDBNDNT_NamedDate1Value_{PROC}{INT} : date == ((java.util.Date) namedObjects.get("{NAMEDDATE1NAME}"))
	) and

	$PCDDBNDNT_NamedDate2_{PROC}{INT} : NamedDate
	(
		name == "{NAMEDDATE2NAME}", 
		date != null,
		$PCDDBNDNT_NamedDate2Value_{PROC}{INT} : date == ((java.util.Date) namedObjects.get("{NAMEDDATE2NAME}"))
	) and
	
	$PCDDBNDNT_ProcIds_{PROC}{INT} : java.util.List (size >= {INT} ) from accumulate 
	( 
		ProcedureConcept
		( 
		openCdsConceptCode == "{PROC}",
		$PCDDBNDNT_ProcTargetId_{PROC}{INT} : conceptTargetId 
		),
	init (ArrayList $PCDDBNDNT_TempProcIds_{PROC}{INT} = new ArrayList(); ),
	action ($PCDDBNDNT_TempProcIds_{PROC}{INT}.add($PCDDBNDNT_ProcTargetId_{PROC}{INT}); ),
	reverse ($PCDDBNDNT_TempProcIds_{PROC}{INT}.remove($PCDDBNDNT_ProcTargetId_{PROC}{INT}); ),
	result($PCDDBNDNT_TempProcIds_{PROC}{INT})
	) and

	$PCDDBNDNT_ProcDates_{PROC}{INT} : java.util.List (size >= {INT} ) from accumulate 
	( 
		$PCDDBNDNT_proc_{PROC}{INT} : ProcedureEvent
		( 
		evaluatedPersonId == $evaluatedPersonId, 
		id memberOf $PCDDBNDNT_ProcIds_{PROC}{INT},
		(stripTimeComponent(procedureTime.get{HIGHLOW}())) {COMP1} (stripTimeComponent($PCDDBNDNT_NamedDate1Value_{PROC}{INT})),
		(stripTimeComponent(procedureTime.get{HIGHLOW}())) {COMP2} (stripTimeComponent($PCDDBNDNT_NamedDate2Value_{PROC}{INT})),
		$PCDDBNDNT_procDate_{PROC}{INT} : procedureTime.get{HIGHLOW}()
		),
	init (ArrayList $PCDDBNDNT_TempProcDates_{PROC}{INT} = new ArrayList(); ),
	action ($PCDDBNDNT_TempProcDates_{PROC}{INT}.add($PCDDBNDNT_procDate_{PROC}{INT})),
	reverse ($PCDDBNDNT_TempProcDates_{PROC}{INT}.remove($PCDDBNDNT_procDate_{PROC}{INT})),
	result($PCDDBNDNT_TempProcDates_{PROC}{INT})
	) and
		
	EvaluatedPerson(eval(countDistinctDates($PCDDBNDNT_ProcDates_{PROC}{INT}) >= {INT}))
) //DslUsed==Proc.Count.Distinct.Date.Between.NamedDates.NoTime.Dsl|||PROC=={PROC}|||HIGHLOW=={HIGHLOW}|||COMP1=={COMP1}|||NAMEDDATE1NAME=={NAMEDDATE1NAME}|||COMP2=={COMP2}|||NAMEDDATE2NAME=={NAMEDDATE2NAME}|||INT=={INT}
