[when] Dem.Gender - Evaluated Person gender is {GENDER:ENUM:GenderConcept.openCdsConceptCode} = 
(
$GenderDsl_evalPerson : EvaluatedPerson(id == $evaluatedPersonId) and GenderConcept(conceptTargetId == $GenderDsl_evalPerson.id, openCdsConceptCode == "{GENDER}")
) //DslUsed==Dem.Gender.Dsl|||GENDER=={GENDER}