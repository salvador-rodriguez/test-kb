[then] Set.EncListId.Add.EncListRefs - Set {ENCLISTID1:ENUM:EncounterListId.reference} as {ENCLISTID2:ENUM:EncounterListId.reference} added to {ENCLISTID3:ENUM:EncounterListId.reference} = 
List {ENCLISTID1} = new ArrayList();
{ENCLISTID1}.addAll({ENCLISTID2});
{ENCLISTID1}.addAll({ENCLISTID3}); //DslUsed==Set.EncListId.Add.EncListRefs.Dsl|||ENCLISTID1=={ENCLISTID1}|||ENCLISTID2=={ENCLISTID2}|||ENCLISTID3=={ENCLISTID3}