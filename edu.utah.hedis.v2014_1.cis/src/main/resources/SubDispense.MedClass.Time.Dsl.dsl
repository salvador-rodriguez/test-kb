[when] SubDispense.MedClass.Time - Evaluated Person had {MEDCLASS:ENUM:MedicationClassConcept.openCdsConceptCode} whose dispensation {HIGHLOW:ENUM:TimeInterval.highLowUpper} time was in the past {INT} {TIMEUNITS:ENUM:EnumerationTarget.javaCalendarUnitNoWeeks} = 
( 
	$SubDispenseMedClassTimeDsl_medClassConcept_{MEDCLASS} : MedicationClassConcept(openCdsConceptCode == "{MEDCLASS}") and 
	
	$SubDispenseMedClassTimeDsl_subDispenseEvent_{MEDCLASS} : SubstanceDispensationEvent
	(
		id == $SubDispenseMedClassTimeDsl_medClassConcept_{MEDCLASS}.conceptTargetId,
		eval(timeBeforeByAtMost(dispensationTime.get{HIGHLOW}(), $evalTime, {INT}, {TIMEUNITS}, namedObjects)) 
	) and

	EvaluatedPerson(eval(flagClinicalStatementToReturnInOutput($SubDispenseMedClassTimeDsl_subDispenseEvent_{MEDCLASS})))	

) //DslUsed==SubDispense.MedClass.Time.Dsl|||SUBADMID=={SUBADMID}|||MEDCLASS=={MEDCLASS}|||HIGHLOW=={HIGHLOW}|||INT=={INT}|||TIMEUNITS=={TIMEUNITS}